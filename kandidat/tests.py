from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from kandidat.forms import RegisterForm
from django.contrib.auth import get_user_model, authenticate, login
from rest_framework.status import HTTP_200_OK,HTTP_302_FOUND


class RegisterViewTest(TestCase):
    def setUp(self):
        # Initialisierung des Testclients
        self.client = Client()

        # URL der zu testenden Ansicht
        self.register_url = reverse('register')

    def test_register_user(self):
        # Erstellen von Testformulardaten
        data = {
            'username': 'testuser',
            'password': 'testpassword123',
            'confirm': 'testpassword123',
        }

        # Senden einer POST-Anfrage an die Ansicht mit den Formulardaten
        response = self.client.post(self.register_url, data)

        # Überprüfen, ob der Benutzer angelegt wurde
        self.assertEqual(get_user_model().objects.count(), 1)

        # Überprüfen, ob die Ansicht nach einer erfolgreichen Anmeldung zur Startseite weiterleitet

        self.assertRedirects(response, reverse('login'))

    def test_register_user_with_password_mismatch(self):
            # Formulardaten erstellen, bei denen das Passwort und die Bestätigung nicht übereinstimmen
            data = {
                'username': 'testuser',
                'password': 'testpassword123',
                'confirm': 'wrongpassword',
            }

            # Senden einer POST-Anfrage an die Ansicht mit den Formulardaten
            response = self.client.post(self.register_url, data)

            # Überprüfen, dass der Benutzer noch nicht angelegt wurde
            self.assertEqual(get_user_model().objects.count(), 0)

            # Überprüfen Sie, ob die Ansicht die richtige Schablone mit dem Fehler zurückgibt
            self.assertTemplateUsed(response, 'kandidat/register.html')
            self.assertTrue(response.context['error'])

            ###Login-Tests ###

class SigninViewTest(TestCase):

    def setUp(self):
        # Initialisierung des Testclients
        self.client = Client()

        # URL der zu testenden Ansicht
        self.signin_url = reverse('login')

        # Erstellen eines Testnutzers
        self.testuser = get_user_model().objects.create(
            username='testuser', password='testpassword123')

    def test_signin_user(self):
        # Erstellen von Testformulardaten
        data = {
            'username': 'testuser',
            'password': 'testpassword123',
        }

        # Senden einer POST-Anfrage an die Ansicht mit den Formulardaten
        response = self.client.post(self.signin_url, data)

        # Überprüfen, ob die Ansicht nach erfolgreicher Anmeldung auf die Startseite weiterleitet
        self.assertEqual(response.status_code, HTTP_200_OK)

    def test_signin_user_with_incorrect_password(self):
            # Formulardaten erstellen, bei denen das Passwort falsch ist
            data = {
                'username': 'testuser',
                'password': 'wrongpassword',
            }

            # Senden einer POST-Anfrage an die Ansicht mit den Formulardaten
            response = self.client.post(self.signin_url, data)

            # Überprüfen Sie, ob die Ansicht die richtige Template mit dem Fehler zurückgibt
            self.assertTemplateUsed(response, 'kandidat/login.html')

                  ###Logout-Tests ###

    def test_signout_user(self):
        # Testbenutzer anmelden
        self.client.login(username='testuser', password='testpassword123')

        # Überprüfen, ob der Nutzer angemeldet ist
        user = authenticate(username='testuser', password='testpassword123')

        # URL der zu testenden Ansicht
        signout_url = reverse('logout')

        # Senden einer GET-Anfrage an die Abmeldeansicht
        response = self.client.get(signout_url)

        # Überprüfen, ob die Ansicht nach dem Abmelden auf die Anmeldeseite weiterleitet
        self.assertEqual(response.status_code, HTTP_302_FOUND)

        # Überprüfen, ob der Nutzer abgemeldet ist
        response = self.client.get(reverse('kandidat-home'))
        self.assertEqual(response.status_code, HTTP_302_FOUND)

