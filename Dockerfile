# Der das Grundbild unserer Anwendung ist
FROM python:3.9.4-alpine

#ist der Name des Maintainers des Dockerfiles
LABEL maintainer="Romaric Yemeli"

#est notre répertoire de travail
WORKDIR /opt

#Image aktualisieren und einige Voraussetzungen installieren
RUN apk update && \
    apk add gcc musl-dev postgresql-dev bash
RUN pip install --upgrade pip

#erlaubt es, alles aus dem aktuellen Verzeichnis in den Arbeitsordner zu kopieren, der /opt ist
COPY . /opt/

#Installeinsatzanforderungen
RUN pip install -r requirements.txt

#Expose application PORT
EXPOSE 80

#Befehl zum Starten unserer Anwendung
CMD ["python","manage.py","runserver","0.0.0.0:80"]